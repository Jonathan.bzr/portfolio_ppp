import React from 'react';
import ProjectBox from './ProjectBox';
import Fromage from '../images/Fromage.png';
import Oracle from '../images/apex-gif.webp';
import Trisomie from '../images/Trisomie.png';
import Skills from "./Skills";


const handleClick = (skill) => {
    console.log(skill)

}

const Projects = () => {
  return (
      <div>
          <h1 className='projectHeading'>Mes <b>projets</b></h1>
          <div className='project'>
              <ProjectBox projectPhoto={Trisomie} projectName="Trisomie21"/>
              <ProjectBox projectPhoto={Fromage} projectName="Fromage"/>
              <ProjectBox projectPhoto={Oracle} projectName="Oracle"/>
          </div>

          <h1 className='SkillsHeading'>Languages & Logiciels</h1>
          <div className='skills'>
              <Skills skill='React' text="React" onClick={handleClick}/>
              <Skills skill='Html' text="HTML" onClick={handleClick}/>
              <Skills skill='Css' text="CSS" onClick={handleClick}/>
              <Skills skill='Mysql' onClick={handleClick}/>
              <Skills skill='Php' onClick={handleClick}/>
              <Skills skill='Docker' text="Docker" onClick={handleClick}/>
              <Skills skill='Cisco' text="Cisco" onClick={handleClick}/>
              <Skills skill='Git' text="Git" onClick={handleClick}/>
              <Skills skill='Linux' text="Linux" onClick={handleClick}/>
          </div>
      </div>
  )
}

export default Projects