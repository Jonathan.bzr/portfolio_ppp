import React from 'react';
import {FaGithub, FaLinkedin} from "react-icons/fa";
import {GrMail} from "react-icons/gr";

const Footer = () => {
  return (
    <footer>
      <h4>Développer par Jonathan Bezara</h4>
      <div className='footerLinks'>
        <a href="https://github.com/jonathan-bzr" target='_blank'><FaGithub/></a>
        <a href="https://www.linkedin.com/in/jonathan-bezara-952897260/" target='_blank'><FaLinkedin/></a>
        <a href='mailTo:jonathan.bezara@hotmail.com' target='_blank'><GrMail/></a>
      </div>
    </footer>
  )
}

export default Footer