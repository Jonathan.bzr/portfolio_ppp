import React from 'react';
import {FaReact} from "react-icons/fa";


const  ProjectBox = ({projectPhoto, projectName}) => {
  const desc = {
    OracleDesc : "Réalisation d'une application sur Apex Oracle pour gérer des données et établir des statiques liés à des tweets de célébrités. Création d'outil et liaison avec une base de donnée.",


    FromageDesc: "Pendant le second semestre en 2022 dans un groupe de quatre, nous avons développés une application en Java pour gérer la vente de fromages. Ce projet a été réalisé sur Eclipse IDE et Windows Builder. J'ai géré le panier, le stock de fromages et l'interface utilisateur.",


    Trisomie21Desc: "Sous la demande d'une cliente, programmer un site web/application web en partant d'un cahier des charges avec la signature de la cliente. À partir de son modèle papier, réussir à numériser tout l'ensemble du système. Programmé en HTML/CSS et PHP",

  }

  let show ='';
  if(desc[projectName + 'Github']===""){
    show="none";
  }
    
  return (
    <div className='projectBox'> 
        <img className='projectPhoto' src={projectPhoto} alt="Project display" /> 
        <div>
            <br />
            <h3>{projectName}</h3>
            <br />
            {desc[projectName + 'Desc']}
            <br />

        </div>
    </div>
  )
}

export default  ProjectBox