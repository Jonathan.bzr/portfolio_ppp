import React from 'react';

const TextBlocks = ({ text }) => {
    return (
        <div className='TextBlock'>
            {text}
        </div>
    );
};

export default TextBlocks;
