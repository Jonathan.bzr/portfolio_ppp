import React, { useState } from 'react';
import Skills from './Skills.js';
import HelpBlock from './HelpBlock.js';
import ProjectBlock from "./ProjectBlock";

const About = () => {
    const [selectedSkill, setSelectedSkill] = useState(null);

    const handleSkillClick = (skill) => {
        setSelectedSkill(skill);
    };

    return (
        <>
            <h1 className='SkillsBUT'>Compétences universitaire</h1>
            <div className='skillsBUT'>
                <Skills text="Réaliser" skill="Réaliser" className="realiser" onClick={handleSkillClick} />
                <Skills text="Optimiser" skill="Optimiser" className="optimiser" onClick={handleSkillClick} />
                <Skills text="Administrer" skill="Administrer" className="administrer" onClick={handleSkillClick} />
                <Skills text="Gérer" skill="Gérer" className="gerer" onClick={handleSkillClick} />
                <Skills text="Conduire" skill="Conduire" className="conduire" onClick={handleSkillClick} />
                <Skills text="Collaborer" skill="Collaborer" className="collaborer" onClick={handleSkillClick} />
            </div>
            {selectedSkill ? (
                <>
                    <div className="block_skill">
                    <HelpBlock skill={selectedSkill} />
                    <ProjectBlock skill={selectedSkill} />
                    </div>
                </>
            ) : (
                <div className="HelpBlock">
                    <p>Aucune compétence sélectionnée.</p>
                </div>
            )}

        </>
    );
};
export default About;
