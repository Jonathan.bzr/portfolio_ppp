import React from 'react';
import {FaReact} from "react-icons/fa";
import {
    SiCisco,
    SiCss3,
    SiDocker, SiGit,
    SiHtml5,
    SiLinux,
    SiMysql,
    SiPhp,
} from "react-icons/si";

const Skills = ({ skill, text, className, onClick }) => {
    const icon = {
        Php: <SiPhp/>,
        React: <FaReact/>,
        Mysql : <SiMysql/>,
        Docker : <SiDocker/>,
        Linux : <SiLinux/>,
        Html: <SiHtml5/>,
        Css : <SiCss3/>,
        Cisco : <SiCisco/>,
        Git : <SiGit/>,
    }

    return (
        <div title={skill} className={`SkillBox ${className}`} onClick={() => onClick(skill)}>
            {icon[skill]}
            <span className="SkillText">{text}</span>
        </div>
    )
}

export default Skills
