import React from 'react';
import Tilt from 'react-parallax-tilt';
import Avatar from '../images/me.png';
import icon from '../images/Avatar.png';


const Home = () => {
  return (
    <div >
      <div className='HomePage'>

        <div className='HomeText'>
          <h1><b>Bonjour !</b></h1>
          <h1>Je suis <b>Jonathan Bezara</b></h1>
          <h1>Bienvenu dans mon portfolio</h1>

        </div>

        <Tilt>
          <img className='Avatar' src={Avatar} alt="" />
        </Tilt>
        
      </div>

      <div className='AboutPage'>
        <div className='AboutText'>
          <h1 className='AboutTextHeading'><b>Présentation</b></h1>
          <p>
            Je suis actuellement en troisième année de Bachelor Universitaire en informatique, spécialisé en Déploiement
            d'Applications Communicantes et Sécurisées (DACS). <br/><br/> Passionné par le <b>développement web</b> et les <b>réseaux</b>,
            je vous invite à découvrir mes projets où je mets en œuvre les compétences acquises pendant mon cursus scolaire.
            Explorez mes réalisations dans le domaine du développement web et des réseaux.<br/><br/>
          </p>
        </div>
        <Tilt>
          <img className='Avatar' src={icon} alt="" />
        </Tilt>
      </div>
    </div>
  )
}

export default Home