import React from 'react';

const HelpBlock = ({ skill }) => {
    let helpText;
    switch (skill) {
        case "Réaliser":
            helpText = (
                <>
                    <ul>
                        <p>Adapter des
                            applications sur un
                            ensemble de supports
                            (embarqué, web, mobile, IoT…)
                        </p> <br/>
                        <li>Élaborer une application informatique.</li>
                        <li>Faire évoluer une application informatique.</li>
                        <li>Maintenir en conditions opérationnelles une application informatique.</li>
                    </ul>
                </>
            );
            break;
        case "Optimiser":
            helpText = (
                <>
                    <ul>
                        <p>
                            Sélectionner les
                            algorithmes adéquats
                            pour répondre à un
                            problème donné.
                        </p> <br/>
                        <li>Améliorer les performances des programmes dans des contextes contraints.</li>
                        <li>Limiter l’impact environnemental d’une application informatique.</li>
                        <li>Mettre en place des applications informatiques adaptées et efficaces.</li>
                    </ul>
                </>
            );
            break;
        case "Administrer":
            helpText = (
                <>
                    <ul>
                        <p>
                            Faire évoluer et
                            maintenir un système
                            informatique
                            communicant en
                            conditions
                            opérationnelles

                        </p> <br/>
                        <li>Déployer une nouvelle architecture technique.</li>
                        <li>Améliorer une infrastructure existante.</li>
                        <li>Sécuriser les applications et les services.</li>
                    </ul>
                </>
            );
            break;
        case "Gérer":
            helpText = (
                <>
                    <ul>
                        <p>
                            Optimiser une base de
                            données, interagir
                            avec une application
                            et mettre en œuvre la
                            sécurité
                        </p> <br/>
                        <li>Lancer un nouveau projet.</li>
                        <li>Sécuriser des données.</li>
                        <li>Exploiter des données pour la prise de décisions.</li>
                    </ul>
                </>
            );
            break;
        case "Conduire":
            helpText = (
                <>
                    <ul>
                        <p>
                            Appliquer une
                            démarche de suivi de
                            projet en fonction des
                            besoins métiers des
                            clients et des
                            utilisateurs
                        </p> <br/>
                        <li>Lancer un nouveau projet.</li>
                        <li>Piloter le maintien d’un projet en condition opérationnelle.</li>
                        <li>Faire évoluer un système d’information.</li>
                    </ul>
                </>
            );
            break;
        case "Collaborer":
            helpText = (
                <>
                    <ul>
                        <p>
                            Manager une équipe
                            informatique
                        </p> <br/>

                        <li>Lancer un nouveau projet.</li>
                        <li>Organiser son travail en relation avec celui de son équipe.</li>
                        <li>Élaborer, gérer et transmettre de l’information.</li>
                    </ul>
                </>
            );
            break;
    }

    return (
        <div className="HelpBlock">
            <h2>{skill}</h2>
            {helpText}
        </div>
    );
};

export default HelpBlock;
