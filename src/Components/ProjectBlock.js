import React from 'react';
import Trisomie from '../images/trisomie_21.jpg';
import bruit_image from '../images/bruit_image.png';
import sae_reseau from '../images/sae_reseau.png';
import apex_oracle from '../images/apex_oracle.png';
import start_bus from '../images/start_bus.png';

const HelpBlock = ({ skill }) => {
    let helpText;
    switch (skill) {
        case "Réaliser":
            helpText = (
                <>
                    <p>
                        <strong> Trisomie 21 <br/> </strong> <br/>
                        C'est le projet le plus concret, car on eu l'occasion de travailler avec cliente.
                        Donc on a pu réaliser un site web pour une association, avec un cahier des charges, et des réunions avec la cliente.
                        C'est notamment grâce à ce projet que j'ai pu apprendre à réaliser un site web, ainsi ce qui m'a permis de décrocher un stage en tant que développeur web.
                    </p>
                    <img className='img_skill' src={Trisomie}/>
                </>
            );
            break;
        case "Optimiser":
            helpText = (
                <>
                    <p>
                        <strong>Algorithme de traitement d'image <br/> </strong> <br/>
                        On devait traiter des images, pour en ressortir des informations.
                        On a donc étudié différents algorithmes, pour en ressortir le plus efficace.
                        On a ensuite pu l'implémenter, et le tester sur des images pour voir les avantages et inconvénients,
                        donc choisir le plus adapté.
                    </p>
                    <img className='img_skill' src={bruit_image}/>
                </>
            );
            break;
        case "Administrer":
            helpText = (
                <>
                    <p>
                        <strong>Evolution d’infrastructure<br/> </strong> <br/>
                        C'est probablement le projet le plus important pour mon parcours DACS.
                        Car on a fait évoluer puis maintenir un système informatique
                        communicant en conditions opérationnelles. Et puis j'ai pu voir à quoi pouvais ressembler une grande infrastructure type entreprise.
                    </p>
                    <img className='img_skill' src={sae_reseau}/>
                </>
            );
            break;
        case "Gérer":
            helpText = (
                <>
                    <p>
                        <strong> Apex oracle <br/> </strong> <br/>
                        On devait manipuler des données, sur des statistiques Twitter.
                        On a donc utilisé Apex Oracle pour gérer les données, et les afficher sous forme de graphique.
                        Ce projet m'a permis de voir une autre aspect de la gestion de données, car ce n'est pas uniquement des requêtes SQL.
                    </p>
                    <img className='img_skill' src={apex_oracle}/>
                </>
            );
            break;
        case "Conduire":
            helpText = (
                <>
                    <p>
                        <strong> Agence de développement web (Start & Bus)<br/> </strong> <br/>
                        C'est un projet dans le quel on vu l'ensemble des étapes pour la création d'une entreprise, ensuite collaborer avec une société de transport.
                        A l'aide l'agence, on pu effectuer un cahier des charges pour faciliter la récolte d'informations afin d’améliorer les circuits des bus.
                    </p>
                    <img className='img_skill' src={start_bus}/>
                </>
            );
            break;
        case "Collaborer":
            helpText = (
                <>
                    <p>
                        <strong>Alternance </strong> <br/>
                        Faire partie d'une équipe de développeur, m'a permis de collaborer avec des personnes ayant des compétences différentes,
                        mais aussi le savoir être en entreprise. En plus de ça je suis dans un open space, ce qui me permet de collaborer avec tout le monde, et de m'adapter à chaque personne.
                        <br/>
                        <br/>
                        <strong>SAE</strong> <br/>
                        Toute mes années de BUT, j'ai été amené à travailler en groupe et j'ai eu la chance de travailler avec des personnes différentes.
                    </p>
                </>
            );
            break;
    }

    return (
        <div className="HelpBlock_p">
            {helpText}
        </div>
    );
};

export default HelpBlock;
